/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.modules.user.converters;

import com.foreach.across.modules.user.business.Role;
import com.foreach.across.modules.user.services.RoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

public class ObjectToRoleConverter implements Converter<Object, Role>
{
	private final ConversionService conversionService;
	private final RoleService roleService;

	public ObjectToRoleConverter( ConversionService conversionService, RoleService roleService ) {
		this.conversionService = conversionService;
		this.roleService = roleService;
	}

	@Override
	public Role convert( Object source ) {
		if ( source instanceof Role ) {
			return (Role) source;
		}

		String roleName = conversionService.convert( source, String.class );

		if ( !StringUtils.isBlank( roleName ) ) {
			return roleService.getRole( roleName );
		}

		return null;
	}
}
